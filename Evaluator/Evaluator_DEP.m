from_datafly = './in/dep/datafly/';
from_mondrian_str = './in/dep/mondrian_strict/';
from_mondrian_rel = './in/dep/mondrian_relaxed/';

out = './out/dep';

KDirList = dir(strcat(from_datafly,'k*'));
KDirList = { KDirList.name };
Ks = [];
for i=1:size(KDirList,2)
   Ks = [ Ks ; str2num(KDirList{i}(2:size(KDirList{i},2))) ];
end
[Ks,I] = sort(Ks);
KDirList = KDirList(I);

files = [];
aux = dir(strcat(from_datafly,KDirList{1},'/','*2.txt'));
files = [ files {aux.name} ];
aux = dir(strcat(from_datafly,KDirList{1},'/','*5.txt'));
files = [ files {aux.name} ];
aux = dir(strcat(from_datafly,KDirList{1},'/','*10.txt'));
files = [ files {aux.name} ];

STATISTICS_DATAFLY = zeros(size(KDirList,2),size(files,2),5);
STATISTICS_MONDRIAN_STR = zeros(size(KDirList,2),size(files,2),5);
STATISTICS_MONDRIAN_REL = zeros(size(KDirList,2),size(files,2),5);
format = '%f';
for i=1:size(KDirList,2)
    for j=1:size(files,2)
        
       name = strcat(from_datafly,KDirList(i),'/',files(j));
       fileID = fopen(name{1},'r');
       V = fscanf(fileID,format);
       STATISTICS_DATAFLY(i,j,:) = V';
       fclose(fileID);
       
       name = strcat(from_mondrian_str,KDirList(i),'/',files(j));
       fileID = fopen(name{1},'r');
       V = fscanf(fileID,format);
       STATISTICS_MONDRIAN_STR(i,j,:) = V';
       fclose(fileID);
       
       name = strcat(from_mondrian_rel,KDirList(i),'/',files(j));
       fileID = fopen(name{1},'r');
       V = fscanf(fileID,format);
       STATISTICS_MONDRIAN_REL(i,j,:) = V';
       fclose(fileID);
    end
end

J = [];
for i=1:size(files,2)
    I = strfind(files{i},'-');
    J = [ J ; str2num(files{i}(I(1)+1:I(2)-1))];
end
[A,I] = sort(J);
files = files(I);
STATISTICS_DATAFLY = STATISTICS_DATAFLY(:,I,:);
STATISTICS_MONDRIAN_STR = STATISTICS_MONDRIAN_STR(:,I,:);
STATISTICS_MONDRIAN_REL = STATISTICS_MONDRIAN_REL(:,I,:);


colors = [];
for i=1:size(Ks,1)
    colors = [colors ; i/size(Ks,1)*ones(1,2)];
end
colors = [rand(size(Ks,1),1)*0.7 , colors];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             DATAFLY GRAPHICS              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure()
hold on
grid on
title('DATAFLY: IDS COMBS (GROUPED BY N)')
xlabel('FILES')
ylabel('IDS')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_DATAFLY(i,:,1)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','DATAFLY_IDS_K'),'epsc');

figure()
hold on
grid on
title('DATAFLY: MIN (GROUPED BY N)')
xlabel('FILES')
ylabel('MIN')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_DATAFLY(i,:,2)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','DATAFLY_MIN_K'),'epsc');

figure()
hold on
grid on
title('DATAFLY: MAX (GROUPED BY N)')
xlabel('FILES')
ylabel('MIN')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_DATAFLY(i,:,3)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','DATAFLY_MAX_K'),'epsc');

figure()
hold on
grid on
title('DATAFLY: MEAN (GROUPED BY N)')
xlabel('FILES')
ylabel('MEAN')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_DATAFLY(i,:,4)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','DATAFLY_MEAN_K'),'epsc');

figure()
hold on
grid on
title('DATAFLY: VARIANCE (GROUPED BY N)')
xlabel('FILES')
ylabel('VAR')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_DATAFLY(i,:,5)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','DATAFLY_VAR_K'),'epsc');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MONDRIAN STRICT GRAPHICS          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure()
hold on
grid on
title('MONDRIAN STR: IDS COMBS (GROUPED BY N)')
xlabel('FILES')
ylabel('IDS')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_STR(i,:,1)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_STR_IDS_K'),'epsc');

figure()
hold on
grid on
title('MONDRIAN STR: MIN (GROUPED BY N)')
xlabel('FILES')
ylabel('MIN')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_STR(i,:,2)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_STR_MIN_K'),'epsc');

figure()
hold on
grid on
title('MONDRIAN STR: MAX (GROUPED BY N)')
xlabel('FILES')
ylabel('MIN')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_STR(i,:,3)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_STR_MAX_K'),'epsc');

figure()
hold on
grid on
title('MONDRIAN STR: MEAN (GROUPED BY N)')
xlabel('FILES')
ylabel('MEAN')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_STR(i,:,4)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_STR_MEAN_K'),'epsc');

figure()
hold on
grid on
title('MONDRIAN STR: VARIANCE (GROUPED BY N)')
xlabel('FILES')
ylabel('VAR')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_STR(i,:,5)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_STR_VAR_K'),'epsc');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MONDRIAN RELAXED GRAPHICS         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure()
hold on
grid on
title('MONDRIAN REL: IDS COMBS (GROUPED BY N)')
xlabel('FILES')
ylabel('IDS')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_REL(i,:,1)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_REL_IDS_K'),'epsc');

figure()
hold on
grid on
title('MONDRIAN REL: MIN (GROUPED BY N)')
xlabel('FILES')
ylabel('MIN')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_REL(i,:,2)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_REL_MIN_K'),'epsc');

figure()
hold on
grid on
title('MONDRIAN REL: MAX (GROUPED BY N)')
xlabel('FILES')
ylabel('MIN')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_REL(i,:,3)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_REL_MAX_K'),'epsc');

figure()
hold on
grid on
title('MONDRIAN REL: MEAN (GROUPED BY N)')
xlabel('FILES')
ylabel('MEAN')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_REL(i,:,4)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_REL_MEAN_K'),'epsc');

figure()
hold on
grid on
title('MONDRIAN REL: VARIANCE (GROUPED BY N)')
xlabel('FILES')
ylabel('VAR')
set(gca,'XLim',[1 size(files,2)],'XTick',1:size(files,2),'XTickLabel',files, 'XTickLabelRotation', 45)
for i=1:size(Ks,1)
    X = 1:size(files,2);
    plot(X',STATISTICS_MONDRIAN_REL(i,:,5)','color', colors(i,:), 'LineWidth', 2)
end;
legend(KDirList)
hold off
saveas(gcf,strcat(out,'/','MONDRIAN_REL_VAR_K'),'epsc');

pause
fclose('all');
close all;