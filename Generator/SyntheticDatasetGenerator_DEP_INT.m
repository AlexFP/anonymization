function [ ] = SyntheticDatasetGenerator_IND_INT( V, A, file, show)
%SYNTHETICDATASETGENERATOR Generate a csv dataset and figures
%of the values distribution
%   func = 'linear','quadratic'
%   V = vector with values to generate each attribute
%   A = number of INT attributes
%   file = name of the output (with no extension)
%   show = 'on' to display the figures, 'off' to not display

    %%%%%%%%%%%%%%
    % GENERATION %
    %%%%%%%%%%%%%%
    
    X = pick(V,A,'or');

    %%%%%%%%%%%
    % WRITING %
    %%%%%%%%%%%

    formatter = '';
    for i=1:A
       formatter = strcat(formatter, '%d , ');
    end
    formatter = formatter(1:size(formatter,2)-1);
    formatter = strcat(formatter,'\n');

    fileID = fopen(strcat(file,'.txt'),'w');
    fprintf(fileID,sprintf('N %d F %d TYPES ',size(X,1),A));
    for i=1:A
        fprintf(fileID,'INT ');
    end
    fprintf(fileID,'IDS ');
    for i=1:A
        fprintf(fileID,'TRUE ');
    end
    fprintf(fileID,'SEP ,\n');
    fprintf(fileID,formatter,X');
    fclose(fileID);

    %%%%%%%%%%%%
    % GRAPHICS %
    %%%%%%%%%%%%

    for i=1:A
        figure('visible',show)
        hold on
        title(strcat('DISTRIBUTION',num2str(i)))
        xlabel('VALUES')

        plot(1:size(X,1),X(:,i)','r.');
        hold off
        saveas(gcf,strcat(file,'_FIG_A_',num2str(i)),'epsc');
    end
end

