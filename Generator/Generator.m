path = './out/';

As = [ 1 , 2, 5, 10 ];
func = 'linear';
for n=3:5
    N = 10^n;
    for a=1:4
        A = As(a);
        file = strcat(path,'indep/','datasetLIN','-',num2str(N),'-',num2str(A));
        SyntheticDatasetGenerator_IND_INT( func, N, A, file, 'off' )
    end
end

pdf = 'quadratic';
for n=3:5
    N = 10^n;
    for a=1:4
        A = As(a);
        file = strcat(path,'indep/','datasetQUAD','-',num2str(N),'-',num2str(A));
        SyntheticDatasetGenerator_IND_INT( func, N, A, file, 'off' )
    end
end

file = strcat(path,'depen/','datasetLIN','-',num2str(300),'-',num2str(2));
V = 1:300;
SyntheticDatasetGenerator_DEP_INT(V,2,file, 'off')
file = strcat(path,'depen/','datasetQUAD','-',num2str(300),'-',num2str(2));
V = V.^2;
SyntheticDatasetGenerator_DEP_INT(V,2,file, 'off')

file = strcat(path,'depen/','datasetLIN','-',num2str(10),'-',num2str(5));
V = 1:10;
SyntheticDatasetGenerator_DEP_INT(V,5,file, 'off')
file = strcat(path,'depen/','datasetQUAD','-',num2str(10),'-',num2str(5));
V = V.^2;
SyntheticDatasetGenerator_DEP_INT(V,5,file, 'off')

file = strcat(path,'depen/','datasetLIN','-',num2str(3),'-',num2str(10));
V = 1:3;
SyntheticDatasetGenerator_DEP_INT(V,10,file, 'off')
file = strcat(path,'depen/','datasetQUAD','-',num2str(3),'-',num2str(10));
V = V.^2;
SyntheticDatasetGenerator_DEP_INT(V,10,file, 'off')