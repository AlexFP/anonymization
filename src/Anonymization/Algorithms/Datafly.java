package Anonymization.Algorithms;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.lang.Math;

// Class which implements Datafly algorithm
public class Datafly extends AnonymizationAlgorithm{

    // Auxiliar data
    private ArrayList<HashMap<String,Integer>> att_counts = null;
    private HashMap<String,ArrayList<Integer>> seq_counts = null;
    private ArrayList<Integer> candidates_to_supp = null;

    private int[] gen_step = null;

    // Constructor
    public Datafly(){}

    // INITIALIZE: Sets the parameters and load the data
    public boolean initialize(int k, String path){
        boolean result = super.initialize(k, path);

        att_counts = null;
        seq_counts = null;
        candidates_to_supp = new ArrayList<>();
        gen_step = new int[getF()];
        for (int i = 0; i < getF(); i++) {
            gen_step[i] = 0;
        }
        
        return result;
    }

    public void execute(){
        datafly();
    }
    
    // DATAFLY ALGORITHM
    private void datafly() {

        int under_k = Integer.MAX_VALUE;
        while(under_k>getK()){
            // STEP 1
            count();

            // STEP 2
            under_k = 0;
            candidates_to_supp.clear();
            for (Map.Entry<String, ArrayList<Integer>> entry : seq_counts.entrySet()) {
                ArrayList<Integer> list = entry.getValue();
                if (list.size() < getK()) {
                    under_k = under_k + list.size();
                    candidates_to_supp.addAll(list);
                }
            }

            // STEP 2.1
            if(under_k>getK()){
                int max = 0;
                int index = -1;
                for(int i=0 ; i<att_counts.size() ; i++){
                    if(!getAttSupp()[i] && att_counts.get(i).size()>max){
                        max = att_counts.get(i).size();
                        index = i;
                    }
                }

                generalize(getQuasi().get(index)); // STEP 2.2
            }
        }

        // STEP 3 & 4
        for (Integer ind : candidates_to_supp) {
            getTupSupp()[ind]=true;
        }
    }

    // COUNT: computes the sequence frequency and per-attribute value frequency
    private void count(){
        att_counts = new ArrayList<>();
        for(int i=0 ; i<getQuasi().size() ; i++){
            att_counts.add(new HashMap<String,Integer>());
        }
        seq_counts = new HashMap<>();

        for(int n=0 ; n<getN() ; n++){
            String key = "";
            for(int i=0 ; i<getQuasi().size() ; i++){
                if(!getAttSupp()[getQuasi().get(i)]) {
                    if (key.equals("")) {
                        key = getTable()[getQuasi().get(i)][n].toString();
                    } else {
                        key += "-" + getTable()[getQuasi().get(i)][n].toString();
                    }

                    Integer count = att_counts.get(i).get(getTable()[getQuasi().get(i)][n].toString());
                    if (count == null) {
                        att_counts.get(i).put(getTable()[getQuasi().get(i)][n].toString(), 1);
                    } else {
                        att_counts.get(i).put(getTable()[getQuasi().get(i)][n].toString(), count + 1);
                    }
                }
            }

            if(!key.equals("")) {
                ArrayList<Integer> list = seq_counts.get(key);
                if (list == null) {
                    list = new ArrayList<>();
                    list.add(n);
                    seq_counts.put(key, list);
                } else {
                    list.add(n);
                    seq_counts.put(key, list);
                }
            }
        }
    }

    // GENERALIZE: depending on the field type, generalizes
    private void generalize(int index){
        boolean supp = true;
        switch (getTypes()[index]) {
            case CHAR:
                // GEN CHAR???
                for (int n = 0; n < getN(); n++) {
                    getTable()[index][n] = "*";
                    if (!getTable()[index][n].toString().equals("*")) {
                        supp = false;
                    }
                }
                getAttSupp()[index] = supp;
                break;
            case INT:
                // 12345 -> 12340 -> 12300 -> 12000 -> 10000 -> 00000 [SUPPRESSED if every tuple has a 0]
                gen_step[index]++;
                for (int n = 0; n < getN(); n++) {
                    getTable()[index][n] = (((Integer) getTable()[index][n]) / (int) (Math.pow(10, gen_step[index]))) * (int) (Math.pow(10, gen_step[index]));
                    if (!getTable()[index][n].toString().equals("0")) {
                        supp = false;
                    }
                }
                getAttSupp()[index] = supp;
                break;
            case DOUBLE:
                // 1234.56 -> 1234.5 -> 1234.0 -> 1230.0 -> 1200.0 -> 1000.0 -> 0000.0 [SUPPRESSED if every tuple has a 0.0]
                boolean reached = false;
                for (int n = 0; n < getN(); n++) {
                    String number = getTable()[index][n].toString();
                    String[] parts = number.split(".");
                    if (gen_step[index] != 0) {
                        gen_step[index]++;
                        parts[0] = "" + ((Integer.parseInt(parts[0])) / (int) (Math.pow(10, gen_step[index]))) * (int) (Math.pow(10, gen_step[index]));
                        parts[1] = "0";
                    } else {
                        if (parts[1].length() == 1) {
                            reached = true;
                            parts[1] = "0";
                        } else {
                            parts[1] = parts[1].substring(0, parts[1].length() - 1);
                        }
                    }

                    Double result = Double.parseDouble(parts[0] + "." + parts[1]);
                    if (result != 0.0) {
                        supp = false;
                    }
                    getTable()[index][n] = result;
                }
                if (reached) {
                    gen_step[index]++;
                }
                getAttSupp()[index] = supp;
                break;
            case DATE:
                // 01/02/1234 -> 02/1234 -> 1234 -> 1230 -> 1200 -> 1000 -> [SUPPRESSED]
                gen_step[index]++;
                for (int n = 0; n < getN(); n++) {
                    switch (gen_step[index]) {
                        case 1:
                            getTable()[index][n] = ((String) getTable()[index][n]).substring(3);
                            break;
                        case 2:
                            getTable()[index][n] = ((String) getTable()[index][n]).substring(3);
                            break;
                        case 3:
                            getTable()[index][n] = "" + (Integer.parseInt((String) getTable()[index][n]) / 10) * 10;
                            break;
                        case 4:
                            getTable()[index][n] = "" + (Integer.parseInt((String) getTable()[index][n]) / 100) * 100;
                            break;
                        case 5:
                            getTable()[index][n] = "" + (Integer.parseInt((String) getTable()[index][n]) / 1000) * 1000;
                            break;
                        case 6:
                            getAttSupp()[index] = true;
                    }
                }
                break;
            case STRING:
                // "abcde" -> "abcd*" -> "abc*" -> "ab*" -> "a*" -> "*" [SUPPRESSED if every tuple has "*"]
                gen_step[index]++;

                int min_lenght = Integer.MAX_VALUE;
                if(gen_step[index]==1) {
                    for (int n = 0; n < getN(); n++) {
                        if (((String) getTable()[index][n]).length() < min_lenght) {
                            min_lenght = ((String) getTable()[index][n]).length();
                        }
                    }
                }
                for (int n = 0; n < getN(); n++) {
                    String s = (String) getTable()[index][n];
                    if(gen_step[index]==1 && s.length()>min_lenght){
                        s = s.substring(0,min_lenght);
                    }
                    if (!s.equals("*")) {
                        if (s.length() > 1) {
                            if (s.charAt(s.length() - 1) == '*') {
                                s = s.substring(0, s.length() - 2) + "*";
                            } else {
                                s = s.substring(0, s.length() - 1) + "*";
                            }

                            if (!s.equals("*")) {
                                supp = false;
                            }
                            getTable()[index][n] = s;
                        } else {
                            getTable()[index][n] = "*";
                        }
                    }
                }
                getAttSupp()[index] = supp;
                break;
        }
    }

    public void printStatistics(String file){
        ArrayList<String> lines = new ArrayList<>();

        System.out.println("\tSuppressed tuples: "+candidates_to_supp.size());
        int min = Integer.MAX_VALUE;
        int max = 0;
        int seqs = 0;
        for (Map.Entry<String, ArrayList<Integer>> entry : seq_counts.entrySet()) {
            ArrayList<Integer> list = entry.getValue();
            if(list.size() >= getK()){
                seqs++;
                if(list.size()<min) {
                    min = list.size();
                }
                if(list.size()>max) {
                    max = list.size();
                }
            }
        }
        System.out.println("\tNumber of combinations: "+seqs);
        lines.add(Double.toString(seqs));
        System.out.println("\tMin k: "+min);
        lines.add(Double.toString(min));
        System.out.println("\tMax k: "+max);
        lines.add(Double.toString(max));

        double mean = (double)(getN()-candidates_to_supp.size())/seqs;
        System.out.println("\tMean k: "+mean);
        lines.add(Double.toString(mean));

        double sum = 0.0;
        for(Map.Entry<String, ArrayList<Integer>> entry : seq_counts.entrySet()) {
            ArrayList<Integer> list = entry.getValue();
            if(list.size() >= getK()) {
                sum += (list.size() - mean) * (list.size() - mean);
            }
        }

        if(seqs==1){
            System.out.println("\tVariance k: "+0);
            lines.add(Double.toString(0.0));
        }
        else{
            System.out.println("\tVariance k: "+sum/(seqs-1));
            lines.add(Double.toString((sum/(seqs-1))));
        }

        try {
            Files.deleteIfExists(Paths.get(file));
            Files.createFile(Paths.get(file));
            Files.write(Paths.get(file), lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        }
        catch(Exception e){
            System.err.println("\t[ERROR]: Cannot write the file "+file+" correctly.");
            e.printStackTrace();
        }
    }
}