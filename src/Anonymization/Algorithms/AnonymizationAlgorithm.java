package Anonymization.Algorithms;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AnonymizationAlgorithm {

    public enum DataType {
        CHAR,   // .
        INT,    // [0-9]+
        DOUBLE, // [0-9]+[.][0-9]+
        DATE,   // [0-9]{2}[/][0-9]{2}[/][0-9]{4}
        STRING  // .+
    }

    private String separator = ",";

    public String getSep(){ return separator; }

    // PARAMETERS
    private int N = 0;                   // Number of rows
    private int f = 0;                   // Number of fields
    private int k = 0;                   // Anonymity constant

    public int getN(){ return N; }
    public int getF(){ return f; }
    public int getK(){ return k; }
    public int setN(int n){ return N=n; }
    public int setF(int f){ return this.f=f; }
    public int setK(int k){ return this.k=k; }

    // DATA
    private DataType[] types = null;
    private boolean[] att_suppressed = null;
    private boolean[] tup_suppressed = null;
    private Object[][] table = null;
    private ArrayList<Integer> quasi;
    private ArrayList<Integer> data;

    public DataType[] getTypes(){ return types; }
    public DataType getType(int i){ return types[i]; }
    public boolean[] getAttSupp(){ return att_suppressed; }
    public boolean[] getTupSupp(){ return tup_suppressed; }
    public Object[][] getTable(){ return table; }
    public Object getElement(int n, int f){ return table[f][n]; }
    public ArrayList<Integer> getQuasi(){ return quasi; }
    public ArrayList<Integer> getData(){ return data; }

    public boolean initialize(int k, String path){

        // Set the variables to default
        separator = ",";
        N = 0;
        f = 0;
        this.k = k;
        types = null;
        att_suppressed = null;
        tup_suppressed = null;
        table = null;
        quasi = null;
        data = null;

        try{
            Scanner file = new Scanner(new FileReader(path));
            System.out.println("\tLoading the header...");
            if(computeHeader(file)){
                System.out.println("\t[SUCCESS]: HEADER loaded correctly.");
            }
            else{
                file.close();
                System.err.println("\t[ERROR]: HEADER not loaded correctly.");
                System.err.println("\t\tHeader format: N [number of tuples] F [number of attributes] TYPES {types of the attributes: CHAR,INT,DOUBLE,DATE,STRING} IDS {is quasi ID?: TRUE,FALSE} SEP [separator]");
                return false;
            }

            System.out.println("\tLoading the data...");
            if(readData(file)){
                System.out.println("\t[SUCCESS]: DATA loaded correctly.");
            }
            else{
                file.close();
                System.err.println("\t[ERROR]: DATA not loaded correctly.");
                return false;
            }

            file.close();
            return true;
        }
        catch(FileNotFoundException e){
            System.err.println("\t[ERROR]: File "+path+" is not found.");
            return false;
        }
    }

    // Read the header of the file
    private boolean computeHeader(Scanner file){
        String tag = file.next();
        if (tag.equals("N")) {
            N = file.nextInt();
        } else {
            return false;
        }

        tag = file.next();
        if (tag.equals("F")) {
            f = file.nextInt();
        } else {
            return false;
        }

        tag = file.next();
        if (tag.equals("TYPES")) {
            types = new DataType[f];
            for (int i = 0; i < f; i++) {
                types[i] = DataType.valueOf(file.next());
            }
        } else {
            return false;
        }

        tag = file.next();
        if (tag.equals("IDS")) {
            quasi = new ArrayList<>();
            data = new ArrayList<>();
            for (int i = 0; i < f; i++) {
                if (file.nextBoolean()) {
                    quasi.add(i);
                } else {
                    data.add(i);
                }
            }
        } else {
            return false;
        }

        tag = file.next();
        if (tag.equals("SEP")) {
            separator = file.next();
        } else {
            return false;
        }

        att_suppressed = new boolean[f];
        for (int i = 0; i < f; i++) {
            att_suppressed[i] = false;
        }

        tup_suppressed = new boolean[N];
        for (int i = 0; i < N; i++){
            tup_suppressed[i] = false;
        }

        file.nextLine();
        return true;
    }

    private boolean readData(Scanner file){
        table = new Object[f][N];
        file.useDelimiter("[\n]");

        for (int n = 0; n < N; n++) {
            Scanner line = new Scanner(file.nextLine());
            line.useDelimiter("["+separator+"\n]");
            for (int i = 0; i < f; i++) {
                String data;
                switch (types[i]) {
                    case CHAR:
                        data = line.next().replaceAll("[ \n\t\r]", "");
                        if (data.length() == 1) {
                            table[i][n] = data.charAt(0);
                        } else {
                            System.err.println("\t\t[ERROR]: " + data + " is not a single character (" + (n + 2) + ")");
                            return false;
                        }
                        break;
                    case INT:
                        data = line.next().replaceAll("[ \n\t\r]", "");
                        try {
                            table[i][n] = Integer.parseInt(data);
                        } catch (NumberFormatException e) {
                            System.err.println("\t\t[ERROR]: " + data + " is not an integer (" + (n + 2) + ")");
                            return false;
                        }
                        break;
                    case DOUBLE:
                        data = line.next().replaceAll("[ \n\t\r]", "");
                        try {
                            table[i][n] = Double.parseDouble(data);
                        } catch (NumberFormatException e) {
                            System.err.println("\t\t[ERROR]: " + data + " is not a double (" + (n + 2) + ")");
                            return false;
                        }
                        break;
                    case DATE:
                        data = line.next().replaceAll("[ \n\t\r]", "");

                        Pattern p = Pattern.compile("\\d{2}/\\d{2}/\\d{4}");
                        Matcher m = p.matcher(data);
                        if (m.matches()) {
                            table[i][n] = data;
                        } else {
                            System.err.println("\t\t[ERROR]: " + data + " is not a date (" + (n + 2) + ")");
                            return false;
                        }
                        break;
                    case STRING:
                        table[i][n] = line.next().replace("\r", "");
                        break;
                }
            }
        }

        return true;
    }

    public boolean writeResults(String path){
        try{
            ArrayList<String> lines = new ArrayList<>();
            for(int n=0 ; n<N ; n++){
                String line = "";
                for(int i=0 ; i<f ; i++){
                    if(!tup_suppressed[n]){
                        line += table[i][n] + ",";
                    }
                }
                if(!line.equals("")) {
                    lines.add(line.substring(0,line.length()-1));
                }
            }
            Files.deleteIfExists(Paths.get(path));
            Files.write(Paths.get(path), lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            return true;
        }
        catch(Exception e){
            System.err.println("\t[ERROR]: Cannot write the file "+path+" correctly.");
            return false;
        }
    }

    public abstract void execute();         //MUST BE IMPLEMENTED IN THE SUBCLASS
    public abstract void printStatistics(String file); //MUST BE IMPLEMENTED IN THE SUBCLASS
}
