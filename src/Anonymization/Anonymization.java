package Anonymization;

import Anonymization.Algorithms.AnonymizationAlgorithm;
import Anonymization.Algorithms.Datafly;
import Anonymization.Algorithms.Mondrian;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Locale;

// Main class, it checks the program arguments and executes the algorithm(initilization, loading, execution and writing)
public class Anonymization {

    public static void main(String[] args){

        boolean datafly;
        Integer k;
        boolean strict = false;
        int starting_index;
        String path = "";
        String path_results = "";
        String path_statistics = "";

        Locale.setDefault(new Locale("en", "US"));
        if(args[0].compareTo("datafly")==0){
            datafly = true;
            starting_index = 2;
            path = args[starting_index];
            path_results = path+"results/"+args[0]+"/";
            path_statistics = path+"statistics/"+args[0]+"/";
            System.out.println("DATAFLY ALGORITHM");
        }
        else if(args[0].compareTo("mondrian")==0){
            datafly = false;
            starting_index = 3;
            path = args[starting_index];
            path_results = path+"results/"+args[0]+"_"+args[2]+"/";
            path_statistics = path+"statistics/"+args[0]+"_"+args[2]+"/";
            if(args[2].compareTo("strict")==0){
                System.out.println("MONDRIAN ALGORITHM (strict)");
                strict = true;
            }
            else if(args[2].compareTo("relaxed")==0){
                System.out.println("MONDRIAN ALGORITHM (relaxed)");
                strict = false;
            }
            else{
                System.err.println(args[2] + " is not a supported mode for mondrian!");
                System.err.println("Use: [program_name] mondrian [k] [\"strict\"|\"soft\"] [input_folder]");
                return;
            }
        }
        else{
            System.err.println(args[0] + " is not a supported algorithm!");
            System.err.println("Use: [program_name] datafly [k] [input_folder]");
            System.err.println("Use: [program_name] mondrian [k] [\"strict\"|\"soft\"] [input_folder]");
            return;
        }

        try{
            k = Integer.parseInt(args[1]);
            path_results = path_results + "k" + k + "/";
            path_statistics = path_statistics + "k" + k + "/";
        }
        catch(NumberFormatException e){
            System.err.println(args[1] + " is not a valid number for k!");
            System.err.println("Use: [program_name] datafly [k] [input_folder]");
            System.err.println("Use: [program_name] mondrian [k] [\"strict\"|\"soft\"] [input_folder]");
            return;
        }

        try{
            File dir = new File(path_results);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            dir = new File(path_statistics);
            if (!dir.exists()) {
                dir.mkdirs();
            }
        }
        catch (Exception e){
            return;
        }

        ArrayList<String> files = getFiles(args[starting_index]);

        for(String file : files){

            System.out.println("\nFile: "+file);
            AnonymizationAlgorithm algorithm;
            if(datafly){
                algorithm = new Datafly();
            }
            else{
                algorithm = new Mondrian(strict);
            }

            System.out.println("Initializing the algorithm and loading the data...");
            long startTime = System.currentTimeMillis();
            if(algorithm.initialize(k,path+file)){
                long endTime = System.currentTimeMillis();
                System.out.println("[DONE] - " + (endTime-startTime) + "ms.");

                System.out.println("Executing...");
                startTime = System.currentTimeMillis();
                algorithm.execute();
                endTime = System.currentTimeMillis();
                System.out.println("[DONE] - " + (endTime-startTime) + "ms.");

                System.out.println("Writing the resulting data...");
                startTime = System.currentTimeMillis();
                if(algorithm.writeResults(path_results+file)){
                    endTime = System.currentTimeMillis();
                    System.out.println("[DONE] - " + (endTime-startTime) + "ms.");
                }

                System.out.println("Resulting statistics:");
                algorithm.printStatistics(path_statistics+file);
            }
        }
    }

    static ArrayList<String> getFiles(String directory) {
        ArrayList<String> textFiles = new ArrayList<String>();
        File dir = new File(directory);
        for (File file : dir.listFiles()) {
            if (file.getName().endsWith((".txt"))) {
                textFiles.add(file.getName());
            }
        }
        return textFiles;
    }
}
