package Anonymization.Algorithms;

import Anonymization.ExtraTools.Partition;
import Anonymization.ExtraTools.Range;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;

// Class which implements Mondrian algorithm
public class Mondrian extends AnonymizationAlgorithm{

    // Auxiliar data
    private boolean strict = false;
    private ArrayList<Partition> finalPartitions = null;

    // Constructor
    public Mondrian(boolean st){
        strict = st;
    }

    // INITIALIZE: Sets the parameters and load the data
    public boolean initialize(int k, String path){
        boolean result = super.initialize(k, path);

        finalPartitions = new ArrayList<>();

        return result;
    }

    public void execute(){
        mondrian();
    }

    public boolean writeResults(String path){
        try{
            ArrayList<String> lines = new ArrayList<>();
            for(int n=0 ; n<this.getN() ; n++){
                String line = "";

                // Looking for the corresponding partition
                Partition part = new Partition();
                for(Partition p: finalPartitions){
                    if(p.hasIndex(n)){
                        part = p;
                        break;
                    }
                }

                for(int i=0 ; i<this.getF() ; i++){
                    Range r = part.getRange(i);
                    if(r!=null){
                        if(r.getMin().equals(r.getMax())){
                            line += r.getMin().toString() + ",";
                        }
                        else{
                            line += "[" + r.getMin().toString() + ":" + r.getMax().toString() + "],";
                        }
                    }
                    else{
                        line += this.getElement(n,i).toString() + ",";
                    }
                }
                if(!line.equals("")) {
                    lines.add(line.substring(0,line.length()-1));
                }
            }
            Files.deleteIfExists(Paths.get(path));
            Files.write(Paths.get(path), lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            return true;
        }
        catch(Exception e){
            System.err.println("\t[ERROR]: Cannot write the file "+path+" correctly.");
            return false;
        }
    }

    // MONDRIAN ALGORITHM
    private void mondrian(){
        // Creation of the partition list
        ArrayList<Partition> partitions = new ArrayList<>();
        // Creation of the first and unique partition containing all the data

        Partition.setTypes(new ArrayList<>(Arrays.asList(this.getTypes())));
        Partition p = new Partition(this.getQuasi());
        for(int i=0 ; i<this.getN() ; i++){
            p.addIndex(i);
        }
        // Computing the ranges of the fields
        p.updateRanges(this.getTable());
        // Adding the partition to the list
        partitions.add(p);

        // Computes the partitions 'til no more partition rest
        while(!partitions.isEmpty()){
            //System.out.println("\tNumber of active partitions: " + partitions.size());
            ArrayList<Partition> parts = computeNext(partitions.remove(0));
            if(parts!=null) {
                partitions.addAll(parts);
            }
        }
        System.out.println("\tTotal final partitions: " + finalPartitions.size());
    }

    // COMPUTENEXT: Splits the partition or adds it to the final partitions if it has not enough elements
    private ArrayList<Partition> computeNext(Partition p){
        ArrayList<Partition> parts = new ArrayList<>();
        if(p.getSize()<2*this.getK())
            finalPartitions.add(p);
        else{
            parts = p.split(this.getTable(), strict, this.getK());
            if(parts==null){
                finalPartitions.add(p);
            }
        }
        return parts;
    }

    public void printStatistics(String file){

        ArrayList<String> lines = new ArrayList<>();

        int min = Integer.MAX_VALUE;
        int max = 0;
        for(int i=0 ; i<finalPartitions.size() ; i++){
            if(finalPartitions.get(i).getSize()<min){
                min = finalPartitions.get(i).getSize();
            }
            if(finalPartitions.get(i).getSize()>max){
                max = finalPartitions.get(i).getSize();
            }
        }
        System.out.println("\tNumber of combinations: "+finalPartitions.size());
        lines.add(Double.toString(finalPartitions.size()));
        System.out.println("\tMin k: "+min);
        lines.add(Double.toString(min));
        System.out.println("\tMax k: "+max);
        lines.add(Double.toString(max));

        double mean = (double)getN()/finalPartitions.size();
        System.out.println("\tMean k: "+mean);
        lines.add(Double.toString(mean));

        double sum = 0.0;
        for(int i=0 ; i<finalPartitions.size() ; i++){
            sum += (finalPartitions.get(i).getSize()-mean)*(finalPartitions.get(i).getSize()-mean);
        }
        System.out.println("\tVariance k: "+sum/(finalPartitions.size()-1));
        lines.add(Double.toString(sum/(finalPartitions.size()-1)));

        try {
            Files.deleteIfExists(Paths.get(file));
            Files.createFile(Paths.get(file));
            Files.write(Paths.get(file), lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("\t[ERROR]: Cannot write the file "+file+" correctly.");
        }
    }
}
