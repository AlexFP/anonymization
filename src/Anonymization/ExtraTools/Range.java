package Anonymization.ExtraTools;

import Anonymization.Algorithms.AnonymizationAlgorithm;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

// Class which implements a range used in Partition
public class Range{

    private int realIndex; // Index of the table which this range belongs to
    private AnonymizationAlgorithm.DataType type; // Type of the elements in this range (CHAR, INT, DOUBLE, DATE or STRING)
    private Object min;
    private Object max;
    private Object median;
    private double norm;

    // RANGE: initializes the data
    public Range(int realIndex, AnonymizationAlgorithm.DataType type, Object low, Object high, Object median, double norm){
        this.realIndex = realIndex;
        this.type = type;
        min = low;
        max = high;
        this.median = median;
        this.norm = norm;
    }

    public void setRealIndex(int realIndex) { this.realIndex = realIndex; }
    public void setType(AnonymizationAlgorithm.DataType type) { this.type = type; }
    public void setMin(Object low){
        min = low;
    }
    public void setMax(Object high){
        max = high;
    }
    public void setMedian(Object med){
        median = med;
    }
    public void setNorm (double norm) { this.norm = norm; }
    public int getRealIndex() { return realIndex; }
    public AnonymizationAlgorithm.DataType getType() { return type; }
    public Object getMin(){
        return min;
    }
    public Object getMax(){
        return max;
    }
    public Object getMedian() { return median; }
    public double getNorm() { return norm; }

    // COMPARE: Returns 0 if o==median, -1 if o>median or 1 if o<median
    public int compare(Object o){
        switch(type){
            case CHAR:
                if(((Character)o).equals((Character)median)){
                    return 0;
                }
                else if((Character)o < (Character)median) {
                    return 1;
                }
                else{
                    return -1;
                }
            case INT:
                if(((Integer)o).equals((Integer)median)){
                    return 0;
                }
                else if((Integer)o < (Integer)median) {
                    return 1;
                }
                else{
                    return -1;
                }
            case DOUBLE:
                if(((Double)o).equals((Double)median)){
                    return 0;
                }
                else if((Double)o < (Double)median) {
                    return 1;
                }
                else{
                    return -1;
                }
            case DATE:
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date d_o = df.parse((String) o);
                    Date d_med = df.parse((String) median);

                    if(d_o.equals(d_med)){
                        return 0;
                    }
                    else if(d_o.before(d_med)) {
                        return 1;
                    }
                    else{
                        return -1;
                    }
                }
                catch (Exception e){ return 0; }
            case STRING:
                if(((String)o).compareTo((String)median)==0){
                    return 0;
                }
                else if(((String)o).compareTo((String)median)<0) {
                    return 1;
                }
                else{
                    return -1;
                }
        }
        return Integer.MIN_VALUE;
    }

    // ISWIDER: Returns true if this range is wider (normalized) than r
    public boolean isWider(Range r){
        if(getWidth()/norm==r.getWidth()/r.getNorm()){
            return norm<r.getNorm();
        }
        else {
            return getWidth()/norm > r.getWidth()/r.getNorm();
        }
    }

    // GETNORMWIDE: Returns a number[0.0,1.0] than representes how much wide is a range. (MAX-MIN)/MAX
    public double getWidth(){
        switch(type){
            case CHAR:
                Character lc = (Character) min;
                Character hc = (Character) max;

                return (double)(hc-lc);
            case INT:
                Integer li = (Integer) min;
                Integer hi = (Integer) max;

                return (double)(hi-li);
            case DOUBLE:
                Double ld = (Double) min;
                Double hd = (Double) max;

                return (hd-ld);
            case DATE:
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date d_min = df.parse((String) min);
                    Date d_max = df.parse((String) max);

                    long days = TimeUnit.DAYS.convert(d_max.getTime()-d_min.getTime(),TimeUnit.MILLISECONDS);
                    return days;
                }
                catch (Exception e){ return 0.0; }
            case STRING:
                char[] ls = ((String) min).toCharArray();
                char[] hs = ((String) max).toCharArray();

                int i=0;
                while(i<hs.length-1 && hs[i]==ls[i]){
                    i++;
                }

                return (double)(hs[i]-ls[i]);
        }
        return 0.0;
    }

}
