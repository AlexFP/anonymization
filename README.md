# ANONYMIZATION #

### Project Structure ###
- src: Java sources with the anonymization algorithms.
    - Anonymization.java: Checks the arguments and execute the choosen algorithm.
    - Algorithms: Folder with the algorithms sources.
        - AnonymizationAlgorithm: Parent class with common methods.
        - Datafly: Datafly algorithm class.
        - Mondrian: Mondrian algorithm class. Uses ExtraTools/Partition.java.
    - ExtraTools: Folder with support classes for algorithms.
        - Partition: A class to simulate a multidimensional partition (used by Mondrian).
        - Range: A class to manage ranges with the indices of the tables, min value, max value... (used by Pratition).
- Generator: Tools to generate datasets. MATLAB sources.
- Evaluator: Tools to draw graphics using the statistics calculated. MATLAB sources.

    
### Generation of the datasets ###
    
- Modify the file Generator to generate the desired set of datasets.
- Execute the Generator.
- Get the datasets from the out folder.

Note: In this work, datasets are divided in 'Dependent' and 'Independent'. If each attribute can be a identifier on its own, the dataset is a 'Independent Dataset' in other case is a 'Dependent Dataset'.

### Execution of the datasets ###
    
- Copy the datasets to a empty folder (for example ./Resources).
- Execute Anonymization with the choosen parameters.
- Get the results from ./Resources/results/algorithm/k/ and the statistics from Resources/statistics/algorithm/k/

### Evaluation of the statistics ###
    
- After execute Datafly, Mondrian strict and Mondrian relaxed with the same datasets and k value, copy everyting within the statistics folder to Evaluator/in/ind (if the attributes are independent) or Evaluator/in/dep.
- Execute Evaluator_DEP or Evaluator_IND.
- Get the graphics from out folder.
     
### Datasets structure ###
    
The structure of the dataset is based on csv format. The first line must contain the header, thats it:
N [n] F [f] TYPES {types} IDS {ids} SEP [sep]
- N is the number of rows/tuples.
- F is the number of columns/attributes.
- TYPES is a list of the types of the attributes, the size must be [f] and the elements must be CHAR, INT, DOUBLE, DATE, STRING.
- IDS is a list of TRUE/FALSE, it means if the attribute is a quasi-identifier or not. The size must be [f].
- SEP is the separator of the values.

After that, each line contains the values of a tuple, separated by [sep].

   Example:
    
    N 5 F 2 TYPES CHAR INT IDS TRUE FALSE SEP ;
    a ; 1
    b ; 1
    c ; 2
    d ; 2
    e ; 2
    
### Anonymization execution parameters ###

    program datafly [k] [folder]
    program mondrian [k] [strict|relaxed] [folder]
    
Example:
    
    program datafly 2 ./Resources   
Executes every dataset in ./Resources with datafly and 2-anonymization.
    
    program mondrian 3 relaxed ./Resources      
Executes every dataset in ./Resources with mondrian relaxed and 3-anonymization.
    
    program mondrian 4 strict ./Resources          
Executes every dataset in ./Resources with mondrian strict and 4-anonymization.